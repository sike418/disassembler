EA_COMPARISONS:

*-------------------- Comparison for the Source -----------------------*

REG_SOURCE_DN
                    MOVE.B          SOURCE_REGISTER,D4
                    CMP.B           #%000,D4
                    BEQ             LOAD_SOURCE_D0          *Load D0
                    CMP.B           #%001,D4
                    BEQ             LOAD_SOURCE_D1          *Load D1
                    CMP.B           #%010,D4
                    BEQ             LOAD_SOURCE_D2          *Load D2
                    CMP.B           #%011,D4
                    BEQ             LOAD_SOURCE_D3          *Load D3
                    CMP.B           #%100,D4
                    BEQ             LOAD_SOURCE_D4          *Load D4
                    CMP.B           #%101,D4
                    BEQ             LOAD_SOURCE_D5          *Load D5
                    CMP.B           #%110,D4
                    BEQ             LOAD_SOURCE_D6          *Load D6
                    CMP.B           #%111,D4
                    BEQ             LOAD_SOURCE_D7          *Load D7  
                    RTS
                    
REG_SOURCE_AN     
                    MOVE.B          SOURCE_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_SOURCE_A0          *Load A0
                    CMP.B          #%001,D4
                    BEQ             LOAD_SOURCE_A1          *Load A1
                    CMP.B          #%010,D4
                    BEQ             LOAD_SOURCE_A2          *Load A2
                    CMP.B          #%011,D4
                    BEQ             LOAD_SOURCE_A3          *Load A3
                    CMP.B          #%100,D4
                    BEQ             LOAD_SOURCE_A4          *Load A4
                    CMP.B          #%101,D4
                    BEQ             LOAD_SOURCE_A5          *Load A5
                    CMP.B          #%110,D4
                    BEQ             LOAD_SOURCE_A6          *Load A6
                    CMP.B          #%111,D4
                    BEQ             LOAD_SOURCE_A7          *Load A7 
                    RTS
                    
REG_SOURCE_IND_ADDR     
                    MOVE.B          SOURCE_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A0          *Load (A0)
                    CMP.B          #%001,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A1          *Load (A1)
                    CMP.B          #%010,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A2          *Load (A2)
                    CMP.B          #%011,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A3          *Load (A3)
                    CMP.B          #%100,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A4          *Load (A4)
                    CMP.B          #%101,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A5          *Load (A5)
                    CMP.B          #%110,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A6          *Load (A6)
                    CMP.B          #%111,D4
                    BEQ             LOAD_SOURCE_IND_ADDR_A7          *Load (A7)
                    RTS 
                       
REG_SOURCE_INCR_ADDR                    
                    MOVE.B          SOURCE_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A0          *Load (A0)+
                    CMP.B          #%001,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A1          *Load (A1)+
                    CMP.B          #%010,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A2          *Load (A2)+
                    CMP.B          #%011,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A3          *Load (A3)+
                    CMP.B          #%100,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A4          *Load (A4)+
                    CMP.B          #%101,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A5          *Load (A5)+
                    CMP.B          #%110,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A6          *Load (A6)+
                    CMP.B          #%111,D4
                    BEQ             LOAD_SOURCE_INCR_ADDR_A7          *Load (A7)+
                    RTS 
                    
REG_SOURCE_DECR_ADDR                    
                    MOVE.B          SOURCE_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A0          *Load -(A0)
                    CMP.B          #%001,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A1          *Load -(A1) 
                    CMP.B          #%010,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A2          *Load -(A2) 
                    CMP.B          #%011,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A3          *Load -(A3) 
                    CMP.B          #%100,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A4          *Load -(A4) 
                    CMP.B          #%101,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A5          *Load -(A5) 
                    CMP.B          #%110,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A6          *Load -(A6) 
                    CMP.B          #%111,D4
                    BEQ             LOAD_SOURCE_DECR_ADDR_A7          *Load -(A7)
                    RTS

*------------------Dn loading--------------------*

LOAD_SOURCE_D0        
                    LEA             DATA_D0,A5
                    RTS
LOAD_SOURCE_D1
                    LEA             DATA_D1,A5
                    RTS
LOAD_SOURCE_D2
                    LEA             DATA_D2,A5                   
                    RTS
LOAD_SOURCE_D3
                    LEA             DATA_D3,A5
                    RTS
LOAD_SOURCE_D4
                    LEA             DATA_D4,A5
                    RTS
LOAD_SOURCE_D5
                    LEA             DATA_D5,A5
                    RTS
LOAD_SOURCE_D6
                    LEA             DATA_D6,A5
                    RTS
LOAD_SOURCE_D7                    
                    LEA             DATA_D7,A5
                    RTS
                    
*------------------An loading--------------------*                    

LOAD_SOURCE_A0        
                    LEA             DATA_A0,A5
                    RTS
LOAD_SOURCE_A1
                    LEA             DATA_A1,A5
                    RTS
LOAD_SOURCE_A2
                    LEA             DATA_A2,A5
                    RTS                   
LOAD_SOURCE_A3
                    LEA             DATA_A3,A5
                    RTS
LOAD_SOURCE_A4
                    LEA             DATA_A4,A5
                    RTS
LOAD_SOURCE_A5
                    LEA             DATA_A5,A5
                    RTS
LOAD_SOURCE_A6
                    LEA             DATA_A6,A5
                    RTS
LOAD_SOURCE_A7                    
                    LEA             DATA_A7,A5
                    RTS
                    
*-----------------(An) loading-------------------*

LOAD_SOURCE_IND_ADDR_A0
                    LEA             DATA_IND_ADDR_A0,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A1
                    LEA             DATA_IND_ADDR_A1,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A2
                    LEA             DATA_IND_ADDR_A2,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A3
                    LEA             DATA_IND_ADDR_A3,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A4
                    LEA             DATA_IND_ADDR_A4,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A5
                    LEA             DATA_IND_ADDR_A5,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A6
                    LEA             DATA_IND_ADDR_A6,A5
                    RTS
LOAD_SOURCE_IND_ADDR_A7
                    LEA             DATA_IND_ADDR_A7,A5
                    RTS
                    
*-----------------(An)+ loading-----------------*

LOAD_SOURCE_INCR_ADDR_A0
                    LEA             DATA_INCR_ADDR_A0,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A1
                    LEA             DATA_INCR_ADDR_A1,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A2
                    LEA             DATA_INCR_ADDR_A2,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A3
                    LEA             DATA_INCR_ADDR_A3,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A4
                    LEA             DATA_INCR_ADDR_A4,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A5
                    LEA             DATA_INCR_ADDR_A5,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A6
                    LEA             DATA_INCR_ADDR_A6,A5
                    RTS
LOAD_SOURCE_INCR_ADDR_A7
                    LEA             DATA_INCR_ADDR_A7,A5
                    RTS
                    
*---------------- -(An) Loading------------------*

LOAD_SOURCE_DECR_ADDR_A0
                    LEA             DATA_DECR_ADDR_A0,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A1
                    LEA             DATA_DECR_ADDR_A1,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A2
                    LEA             DATA_DECR_ADDR_A2,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A3
                    LEA             DATA_DECR_ADDR_A3,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A4
                    LEA             DATA_DECR_ADDR_A4,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A5
                    LEA             DATA_DECR_ADDR_A5,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A6
                    LEA             DATA_DECR_ADDR_A6,A5
                    RTS
LOAD_SOURCE_DECR_ADDR_A7
                    LEA             DATA_DECR_ADDR_A7,A5
                    RTS

*--------------------DESTINATION-------------------------*
REG_DEST_COMP_DN     
                    MOVE.B          DESTINATION_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_DEST_D0          *Load D0
                    CMP.B          #%001,D4
                    BEQ             LOAD_DEST_D1          *Load D1
                    CMP.B          #%010,D4
                    BEQ             LOAD_DEST_D2          *Load D2
                    CMP.B          #%011,D4
                    BEQ             LOAD_DEST_D3          *Load D3
                    CMP.B          #%100,D4
                    BEQ             LOAD_DEST_D4          *Load D4
                    CMP.B          #%101,D4
                    BEQ             LOAD_DEST_D5          *Load D5
                    CMP.B          #%110,D4
                    BEQ             LOAD_DEST_D6          *Load D6
                    CMP.B          #%111,D4
                    BEQ             LOAD_DEST_D7          *Load D7    
                    RTS               

REG_DEST_COMP_AN     
                    MOVE.B          DESTINATION_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_DEST_A0          *Load A0
                    CMP.B          #%001,D4
                    BEQ             LOAD_DEST_A1          *Load A1
                    CMP.B          #%010,D4
                    BEQ             LOAD_DEST_A2          *Load A2
                    CMP.B          #%011,D4
                    BEQ             LOAD_DEST_A3          *Load A3
                    CMP.B          #%100,D4
                    BEQ             LOAD_DEST_A4          *Load A4
                    CMP.B          #%101,D4
                    BEQ             LOAD_DEST_A5          *Load A5
                    CMP.B          #%110,D4
                    BEQ             LOAD_DEST_A6          *Load A6
                    CMP.B          #%111,D4
                    BEQ             LOAD_DEST_A7          *Load A7    
                    RTS     
                    
REG_DEST_COMP_IND_ADDR     
                    MOVE.B          DESTINATION_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_DEST_IND_ADDR_A0          *Load (A0)
                    CMP.B          #%001,D4
                    BEQ             LOAD_DEST_IND_ADDR_A1          *Load (A1)
                    CMP.B          #%010,D4
                    BEQ             LOAD_DEST_IND_ADDR_A2          *Load (A2)
                    CMP.B          #%011,D4
                    BEQ             LOAD_DEST_IND_ADDR_A3          *Load (A3)
                    CMP.B          #%100,D4
                    BEQ             LOAD_DEST_IND_ADDR_A4          *Load (A4)
                    CMP.B          #%101,D4
                    BEQ             LOAD_DEST_IND_ADDR_A5          *Load (A5)
                    CMP.B          #%110,D4
                    BEQ             LOAD_DEST_IND_ADDR_A6          *Load (A6)
                    CMP.B          #%111,D4
                    BEQ             LOAD_DEST_IND_ADDR_A7          *Load (A7) 
                    RTS
                       
REG_DEST_COMP_INCR_ADDR                    
                    MOVE.B          DESTINATION_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A0          *Load (A0)+
                    CMP.B          #%001,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A1          *Load (A1)+
                    CMP.B          #%010,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A2          *Load (A2)+
                    CMP.B          #%011,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A3          *Load (A3)+
                    CMP.B          #%100,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A4          *Load (A4)+
                    CMP.B          #%101,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A5          *Load (A5)+
                    CMP.B          #%110,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A6          *Load (A6)+
                    CMP.B          #%111,D4
                    BEQ             LOAD_DEST_INCR_ADDR_A7          *Load (A7)+ 
                    RTS
                    
REG_DEST_COMP_DECR_ADDR                    
                    MOVE.B          DESTINATION_REGISTER,D4
                    CMP.B          #%000,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A0          *Load -(A0)
                    CMP.B          #%001,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A1          *Load -(A1) 
                    CMP.B          #%010,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A2          *Load -(A2) 
                    CMP.B          #%011,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A3          *Load -(A3) 
                    CMP.B          #%100,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A4          *Load -(A4) 
                    CMP.B          #%101,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A5          *Load -(A5) 
                    CMP.B          #%110,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A6          *Load -(A6) 
                    CMP.B          #%111,D4
                    BEQ             LOAD_DEST_DECR_ADDR_A7          *Load -(A7)
                    RTS
                     
*------------------Dn loading--------------------*

LOAD_DEST_D0        
                    LEA             DATA_D0,A6
                    RTS
LOAD_DEST_D1
                    LEA             DATA_D1,A6
                    RTS
LOAD_DEST_D2
                    LEA             DATA_D2,A6                   
                    RTS
LOAD_DEST_D3
                    LEA             DATA_D3,A6
                    RTS
LOAD_DEST_D4
                    LEA             DATA_D4,A6
                    RTS
LOAD_DEST_D5
                    LEA             DATA_D5,A6
                    RTS
LOAD_DEST_D6
                    LEA             DATA_D6,A6
                    RTS
LOAD_DEST_D7                    
                    LEA             DATA_D7,A6
                    RTS
                    
*------------------An loading--------------------*                    

LOAD_DEST_A0        
                    LEA             DATA_A0,A6
                    RTS
LOAD_DEST_A1
                    LEA             DATA_A1,A6
                    RTS
LOAD_DEST_A2
                    LEA             DATA_A2,A6
                    RTS                   
LOAD_DEST_A3
                    LEA             DATA_A3,A6
                    RTS
LOAD_DEST_A4
                    LEA             DATA_A4,A6
                    RTS
LOAD_DEST_A5
                    LEA             DATA_A5,A6
                    RTS
LOAD_DEST_A6
                    LEA             DATA_A6,A6
                    RTS
LOAD_DEST_A7                    
                    LEA             DATA_A7,A6
                    RTS
                    
*-----------------(An) loading-------------------*

LOAD_DEST_IND_ADDR_A0
                    LEA             DATA_IND_ADDR_A0,A6
                    RTS
LOAD_DEST_IND_ADDR_A1
                    LEA             DATA_IND_ADDR_A1,A6
                    RTS
LOAD_DEST_IND_ADDR_A2
                    LEA             DATA_IND_ADDR_A2,A6
                    RTS
LOAD_DEST_IND_ADDR_A3
                    LEA             DATA_IND_ADDR_A3,A6
                    RTS
LOAD_DEST_IND_ADDR_A4
                    LEA             DATA_IND_ADDR_A4,A6
                    RTS
LOAD_DEST_IND_ADDR_A5
                    LEA             DATA_IND_ADDR_A5,A6
                    RTS
LOAD_DEST_IND_ADDR_A6
                    LEA             DATA_IND_ADDR_A6,A6
                    RTS
LOAD_DEST_IND_ADDR_A7
                    LEA             DATA_IND_ADDR_A7,A6
                    RTS
                    
*-----------------(An)+ loading-----------------*

LOAD_DEST_INCR_ADDR_A0
                    LEA             DATA_INCR_ADDR_A0,A6
                    RTS
LOAD_DEST_INCR_ADDR_A1
                    LEA             DATA_INCR_ADDR_A1,A6
                    RTS
LOAD_DEST_INCR_ADDR_A2
                    LEA             DATA_INCR_ADDR_A2,A6
                    RTS
LOAD_DEST_INCR_ADDR_A3
                    LEA             DATA_INCR_ADDR_A3,A6
                    RTS
LOAD_DEST_INCR_ADDR_A4
                    LEA             DATA_INCR_ADDR_A4,A6
                    RTS
LOAD_DEST_INCR_ADDR_A5
                    LEA             DATA_INCR_ADDR_A5,A6
                    RTS
LOAD_DEST_INCR_ADDR_A6
                    LEA             DATA_INCR_ADDR_A6,A6
                    RTS
LOAD_DEST_INCR_ADDR_A7
                    LEA             DATA_INCR_ADDR_A7,A6
                    RTS
                    
*---------------- -(An) Loading------------------*

LOAD_DEST_DECR_ADDR_A0
                    LEA             DATA_DECR_ADDR_A0,A6
                    RTS
LOAD_DEST_DECR_ADDR_A1
                    LEA             DATA_DECR_ADDR_A1,A6
                    RTS
LOAD_DEST_DECR_ADDR_A2
                    LEA             DATA_DECR_ADDR_A2,A6
                    RTS
LOAD_DEST_DECR_ADDR_A3
                    LEA             DATA_DECR_ADDR_A3,A6
                    RTS
LOAD_DEST_DECR_ADDR_A4
                    LEA             DATA_DECR_ADDR_A4,A6
                    RTS
LOAD_DEST_DECR_ADDR_A5
                    LEA             DATA_DECR_ADDR_A5,A6
                    RTS
LOAD_DEST_DECR_ADDR_A6
                    LEA             DATA_DECR_ADDR_A6,A6
                    RTS
LOAD_DEST_DECR_ADDR_A7
                    LEA             DATA_DECR_ADDR_A7,A6
                    RTS
*--------------------------------------ABS ADDR---------------------------------*
LOAD_DEST_ABS_ADD_W
    MOVE.W     (A1)+,D7      *GRABS EFFECTIVE ADDRESS
    ADDA.W     #2,A2
    MOVEA.L      #ABSOLUTE_DESTINATION,A6
    MOVE.B     #$24,(A6)+       *ADDS $ SYMBOL TO THE FRONT
    
     MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+

    MOVE.B      #$0,(A6)+
    MOVEA.L     #ABSOLUTE_DESTINATION,A6

    RTS
LOAD_DEST_ABS_ADD_L
    MOVE.L     (A1)+,D7
    ADDA.W     #4,A2
    
    MOVEA.L    #ABSOLUTE_DESTINATION,A6 
    MOVE.B     #$24,(A6)+       *ADDS $ SYMBOL TO THE FRONT
    
    SWAP       D7
    
    MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+
    
    SWAP        D7
    
    MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A6)+

    MOVE.B      #$0,(A6)+
    MOVEA.L     #ABSOLUTE_DESTINATION,A6
    RTS
LOAD_SOURCE_ABS_ADD_W
    MOVE.W     (A1)+,D7      *GRABS EFFECTIVE ADDRESS
    ADDA.W     #2,A2
    
    MOVE.L      #ABSOLUTE_SOURCE,A5
    MOVE.B     #$24,(A5)+       *ADDS $ SYMBOL TO THE FRONT
        
    MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+

    MOVE.B      #$0,(A5)+
    MOVEA.L     #ABSOLUTE_SOURCE,A6

    RTS
LOAD_SOURCE_ABS_ADD_L
    MOVE.L     (A1)+,D7
    ADDA.W     #4,A2
    
    MOVEA.L    #ABSOLUTE_SOURCE,A5 
    
    MOVE.B     #$24,(A5)+       *ADDS $ SYMBOL TO THE FRONT
    SWAP       D7
    
    MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+
    
    SWAP        D7
    
    MOVE.W      D7,D2
    MOVE.B     #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+

    MOVE.B      #$0,(A5)+
    MOVEA.L     #ABSOLUTE_SOURCE,A5
    RTS
    
LOAD_SOURCE_IMMEDIATE_DATA
    MOVE.W     (A1)+,D7
    ADDA.W     #2,A2
    
    MOVEA.L    #ABSOLUTE_SOURCE,A5 
    
    MOVE.B     #$23,(A5)+       *ADDS 4 SYMBOL TO THE FRONT
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_4,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    MOVE.B      #NUM_8,D5
    LSL         D5,D2
    MOVE.B      #NUM_12,D5
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+
    
    MOVE.W      D7,D2
    LSL         D5,D2       *STILL CONTAINS 12
    LSR         D5,D2
    JSR         CONVERTTOASCII      *CONVERTS ISOLATED HEX DIGIT TO ASCII
    MOVE.B      D3,(A5)+

    MOVE.B      #$0,(A5)+
    MOVEA.L     #ABSOLUTE_SOURCE,A5

    
    
    RTS

*----------------------------------SIZE--------------------------------*
LOAD_SIZE_BYTE
    LEA     SIZE_B,A4
    RTS
LOAD_SIZE_WORD
    LEA     SIZE_W,A4
    RTS
LOAD_SIZE_LONG
    LEA     SIZE_L,A4
    RTS
*------------------------------INVALID--------------------------------*
INVALID_BITS
    CLR     D6
    RTS
HANDLE_NUM_SHIFT
    MOVE.B SOURCE_REGISTER,D4
    CMP.B   #%000,D4
    BEQ LOAD_N0 *Load Number #0
    CMP.B   #%001,D4
    BEQ LOAD_N1 *Load Number #1
    CMP.B   #%010,D4
    BEQ LOAD_N2 *Load Number #2
    CMP.B   #%011,D4
    BEQ LOAD_N3 *Load Number #3
    CMP.B   #%100,D4
    BEQ LOAD_N4 *Load Number #4
    CMP.B   #%101,D4
    BEQ LOAD_N5 *Load Number #5
    CMP.B   #%110,D4
    BEQ LOAD_N6 *Load Number #6
    CMP.B   #%111,D4
    BEQ LOAD_N7 *Load Number #7 

LOAD_N0
    LEA DATA_N0,A5
    RTS
LOAD_N1
    LEA DATA_N1,A5
    RTS
LOAD_N2
    LEA DATA_N2,A5
    RTS
LOAD_N3
    LEA DATA_N3,A5
    RTS
LOAD_N4
    LEA DATA_N4,A5
    RTS
LOAD_N5
    LEA DATA_N5,A5
    RTS
LOAD_N6
    LEA DATA_N6,A5
    RTS
LOAD_N7
    LEA DATA_N7,A5
    RTS













*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
