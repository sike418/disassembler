IO_SUBROUTINE:

* Register Usage
*A0: Store address location program is looking at
*A1: Used for input/OUTPUT
*A2: Amount IO will need to increment the address location
*A3: Stores OPCODE MESSAGE
*A4: Stores Size MESSAGE
*A5: Stores Source Operand
*A6: Stores Destination Operand

*Assuming the address is Word length at max

* PROMPTS THE USER WITH THE WELCOME MESSAGE
INTRO  
       LEA     INTRO_MESSAGE, A1
       MOVE.B  #14, D0
       TRAP    #15
            
* ASKS USER FOR THE STARTING ADDRESS
* STARTING ADDRESS WILL BE STORED IN A0
INPUT_START 
       CLR     D5
       LEA     INPUT_PROMPTA, A1
       MOVE.B  #14, D0
       TRAP    #15
            
       MOVEA.L #INPUT_STORAGE, A1           * Takes String Input and Stores at $6000
       MOVE.B  #2, D0
       TRAP    #15
            
                        
       CMP     #6,D1
       BGT     INVALIDSTARTINPUT                *CHECKS SIZE OF INPUT DATA MUST BE LESS THAN 6 CHARACTERS
            
       JSR     FROMASCII
            
       CMP     #1,D5                       *SHOULD BE EMPTY CHECK IF INVALID INPUT
       BEQ     INVALIDSTARTINPUT
           
       MOVEA.L D7, A0                       * Move and Store starting address at A0
       CLR     D7                           * temporary data register D5 and D6 cleared for later use
            
* ASKS USER FOR THE ENDING ADDRESS
* ENDING ADDRESS WILL BE STORED IN A VARIABLE
INPUT_END   
       CLR     D5                  *RESETS D5
       LEA     INPUT_PROMPTB, A1
       MOVE.B  #14, D0
       TRAP    #15
            
       MOVEA.L #INPUT_STORAGE, A1           * Takes String input and Stores at $6000
       MOVE.B  #2, D0            
       TRAP    #15
            
       CMP     #6,D1
       BGT     INVALIDENDINPUT
       JSR     FROMASCII
            
       CMP     #1,D5                       *SHOULD BE EMPTY CHECK IF INVALID INPUT
       BEQ     INVALIDENDINPUT            
            
       MOVE.L  D7,ENDING_ADDRESS                        * move ending address to VARIABLE
       CLR     D7                           * clear temporary data register at D5 
       BRA     INPUTCONFIRMATION
            
INPUTCONFIRMATION
        CMPA.L    ENDING_ADDRESS,A0
        BGT     ENDINGLESSTHANCASE
        BRA     LOOP
ENDINGLESSTHANCASE
        LEA     INVALIDINPUTMESSAGE2,A1
        MOVE.B  #14,D0
        TRAP    #15     
        BRA     INPUT_START
INVALIDSTARTINPUT
        LEA     INVALIDINPUTMESSAGE,A1
        MOVE.B  #14,D0
        TRAP    #15
        BRA     INPUT_START
INVALIDENDINPUT
        LEA     INVALIDINPUTMESSAGE,A1
        MOVE.B  #14,D0
        TRAP    #15
        BRA     INPUT_END
INVALIDINPUT
        MOVE.B  #1,D5       
        RTS
    
LOOP        
        MOVEA.L #$0,A7
        MOVEA.W #ENDING_ADDRESS,A7
        CMPA.L  (A7),A0
        BGT     RESTART                      *BRANCHES TO THE ENDING BRANCH IF THE CURRENT ADDRESS IS LARGER THAN THE ENDING ADDRESS
        MOVEA.L #$0,A7
            
        MOVEA.L #$0,A1                       *SETS A1 TO 0
        JSR     OP_SUBROUTINE
            
        MOVE.W  A0,D0
        JSR     TOASCII                     *CONVERTS MEMORY ADDRESS TO ASCII CODE FOR PRINTING
        JSR     LOAD
            
        ADDA.W  A2,A0                       *INCREMENTS ADDRESS LOCATION
            
        MOVE.B  #5,D0
        TRAP    #15
            
        BRA     LOOP
            
LOAD    
        CMP     #INVALID,A3
        BEQ     INVALID_CASE
        CMP     #$0,A3
        BNE     OUTPUT
        LEA     INVALID,A3
        BRA     INVALID_CASE
INVALID_CASE
        SUBA.L  A4,A4
        SUBA.L  A5,A5 *CLEARS ADDRESS REGISTER  
        SUBA.L   A6,A6 
        *I NEED TO CONVERT THE ASSEMBLED CODE INTO ASCII      
        BRA     OUTPUT
RESTART 
        LEA     RESTART_MESSAGE, A1         * If user wants to restart the program again
        MOVE.B  #14, D0
        TRAP    #15
        MOVE.B  #4, D0
        TRAP    #15
        CMP.B   #0, D1
        BEQ     INTRO                       * if same, restart the program
        BLT     GOODBYE                     * else end the program
        BGT     GOODBYE

GOODBYE 
        LEA     END_MESSAGE, A1
        MOVE.B  #14, D0
        TRAP    #15
            
OUTPUT      
        MOVE.B  #14,D0          *OUTPUTS MEMORY LOCATION
        TRAP    #15
            
        LEA     SPACE1,A1       *OUTPUTS 5 SPACES
        TRAP    #15
            
        MOVEA.L A3,A1           *OUTPUTS OPCODE
        TRAP    #15             

        *CMPA.L  #A3,INVALID  *CHECKS IF THE PROGRAM COULD IDENTIFY THE OPCODE
        *BEQ     DISPLAYHEX
            
        JSR     TESTSIZE

        LEA     SPACE1,A1
        TRAP    #15
            
        JSR     TESTARGS
            
*            LEA     NEWLINE,A1
*            TRAP    #15
            
        MOVEA.L #$0,A3
            
        SUBA.L  A3,A3
        SUBA.L  A4,A4       *EMPTIES THE CONTENTS OF ADDRESS REGISTERS A3 -> A6
        SUBA.L  A5,A5
        SUBA.L  A6,A6
            
        MOVE.L  #$0,ABSOLUTE_SOURCE
        MOVE.L  #$0,ABSOLUTE_DESTINATION
            
        RTS
        SIMHALT
            
DISPLAYHEX
            *DO SOMETHING
        RTS  
TESTSIZE
        CMPA    #$0,A4
        BNE     DISPLAYSIZE
        RTS
DISPLAYSIZE
        MOVEA.L   A4,A1           *OUTPUTS SIZE IF NECESSARY
        TRAP      #15
        RTS
TESTARGS
        CMPA    #$0,A5
        BEQ     TESTDESTINATION
        
        CMPA    #$0,A6
        BEQ     DISPLAYSOURCE
        
        JSR     DISPLAYSOURCE
        LEA     COMMA,A1        *DISPLAYS COMMA ON CONSOLE
        TRAP    #15
        JSR     DISPLAYDESTINATION 
        RTS
DISPLAYSOURCE
        MOVEA.L     A5,A1
        TRAP        #15
        RTS
TESTDESTINATION
        CMPA     #$0,A6
        BNE     DISPLAYDESTINATION
        RTS
DISPLAYDESTINATION
        MOVEA.L     A6,A1           *DISPLAYS DESTINATION ON CONSOLE
        TRAP        #15
        RTS          
TOASCII
        CLR         D2
        CLR         D3
        CLR         D5
    
        MOVE.W      D0,D2           *CREATES A COPY OF SAID ADDRESS
        MOVE.B      #SHIFT1,D5       *STORES THE DISTANCE TO SHIFT
        LSR         D5,D2               *ISOLATES FIRST CHARACTER
        JSR         CONVERTTOASCII
    
        MOVE.B      #SHIFT2,D5          *SHIFTS THE CONVERTED CHARACTER OVER 8 BITS
        LSL         D5,D3
    
        MOVE.W      D0,D2           *RELOADS THE FULL ADDRESS
        LSL         #4,D2           *ISOLATES SECOND CHARACTER
        MOVE.B      #SHIFT1,D5
        LSR         D5,D2
        JSR         CONVERTTOASCII
    
        SWAP        D3              *MOVES THE FIRST HALF OF THE ADDRESS INTO THE SECOND 
    
        MOVE.W      D0,D2           *RELOADS ADDRESS
        MOVE.B      #SHIFT2,D5
        LSL         D5,D2           
        MOVE.B      #SHIFT1,D5      *ISOLATES THIRD CHARACTER
        LSR         D5,D2           
    
        JSR         CONVERTTOASCII
    
        MOVE.W      D0,D2           *ISOLATES LAST CHARACTER
        LSL         D5,D2
        LSR         D5,D2
    
        MOVE.B      #SHIFT2,D5
        LSL         D5,D3      
        JSR         CONVERTTOASCII
    
        MOVE.L     D3,(CURRENT_ADDRESS)
        MOVEA.W     #CURRENT_ADDRESS,A1
        RTS
    
        SIMHALT             ; halt simulator
    
CONVERTTOASCII
        MOVE.B  #$A,D4
        CMP     D2,D4
        BGT     ONETONINE
        BRA     LETTERS
ONETONINE
        MOVE.B  #FIRSTNINE,D3   *MOVES VALUE USED TO CONVERT TO ASCII INTO D3
        ADD.B   D2,D3           *ADDS HEX VALUE TO D3
        RTS
LETTERS
        MOVE.B  #UPPERCASE,D3  *MOVES VALUES USED TO CONVERT TO ASCII INTO D3
        ADD.B   D2,D3           *ADDS HEX VALUE TO D3
        RTS
FROMASCII
        CMP #0,D1
        BEQ RETURN
    
        JSR TESTREMAINING
    
        LSL #4,D7
        MOVE.B  (A1)+,D2
    
        JSR     CONVERTFROMASCII
    
        SUB.B   #1,D1
        ADD.B   D2,D7
        BRA     FROMASCII
TESTREMAINING
        CMP #4,D1
        BEQ SWAPDATA
        RTS
SWAPDATA
        SWAP    D7 
        RTS
CONVERTFROMASCII
        MOVE.B  #$30,D4
        CMP     D2,D4
        BGT     INVALIDINPUT
    
        MOVE.B  #$40,D4
        CMP     D2,D4
        BGT     FROMONETONINE
    
        MOVE.B  #$46,D4
        CMP     D2,D4
        BGT     FROMUPPERCASE
    
        MOVE.B  #$60,D4
        CMP     D2,D4
        BGT     INVALIDINPUT
    
        MOVE.B  #$67,D4
        CMP     D2,D4
        BGT     FROMLOWERCASE
    
        BRA     INVALIDINPUT 

FROMONETONINE
        MOVE.B  #FIRSTNINE,D3
        SUB.B   D3,D2
        RTS 
FROMUPPERCASE
        MOVE.B  #UPPERCASE,D3
        SUB.B   D3,D2
        RTS
FROMLOWERCASE
        MOVE.B  #LOWERCASE,D3
        SUB.B   D3,D2
        RTS   
   
        SIMHALT             ; halt simulator


*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
